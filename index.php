<?php
require "vendor/autoload.php";
$app = new \Slim\App();
$app->get('/customers/{number}', function($request, $response,$args){
    $str = file_get_contents('ex1.json');
    $array = (json_decode($str,true));
    if(array_key_exists($args['number'], $array)){       	
        return $response->write('Customer name is: '.$array[$args['number']]);
    }
	else{
		return $response->write('Customer was not found :(');
	}

	
    
});
$app->run();